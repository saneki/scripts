# Paths
DSTPATH := "$(HOME)/.bin"
SRCPATH := "scripts"

# Utilities
STOW := stow

all: install

install: FORCE
	$(STOW) -d . -t $(DSTPATH) -S $(SRCPATH)

uninstall: FORCE
	$(STOW) -d . -t $(DSTPATH) -D $(SRCPATH)

FORCE:
