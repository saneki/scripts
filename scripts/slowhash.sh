#!/usr/bin/env bash

# Script for hashing sequentially. Only useful for benchmarking alongside hash.sh.

[ $# -ge 1 ] || { echo "usage: $0 <file>"; exit; }

file="$1"

hashes=( b2sum cksum md5sum sha1sum sha224sum sha256sum sha384sum sha512sum )

slowhash_all() {
	for name in "${hashes[@]}"; do
		output="$(cat "$file" | "$name" 2>/dev/null | cut -d ' ' -f 1)"
		[ -z "$output" ] || printf "%11s %s\n" "$name:" "$output"
	done
}

slowhash_all | sort
