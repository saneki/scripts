#!/usr/bin/env bash
# Chomps CR or LF if found at file's EOF

[ $# -ge 1 ] || { echo "usage: $0 <file>" 1>&2; exit; }

srcfile="$1"
tmpfile="$(mktemp)"

[ "$tmpfile" ] || { echo "error: unable to create temp file" 1>&2; exit 1; }

# https://stackoverflow.com/a/12579554
# Also using `tr` to treat \r the same as \n
if [ "$(tail -c 1 "$srcfile" | tr '\r' '\n' | wc -l)" == "1" ]; then
	head -c -1 "$srcfile" > "$tmpfile"
	mv "$tmpfile" "$srcfile"
fi
