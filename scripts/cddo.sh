#!/usr/bin/env bash

echo_usage() {
	echo "usage: $0 [-h] [-m] <directory> [command...]"
}

echo_help() {
	echo_usage
	echo
	echo 'Changes directory and runs a command.'
	echo
	echo '  -h    Show this help message.'
	echo '  -m    Make the directory if necessary.'
}

help=0
makedir=0
usage=0

while getopts ':hm' opt; do
	case ${opt} in
		h) help=1 ;;
		m) makedir=1 ;;
		\?) usage=1 ;;
	esac
done
shift $((OPTIND -1))

# Show help and exit if specified
[ "$help" == '0' ] || { echo_help 1>&2; exit; }

# Show usage and exit if unknown argument
[ "$usage" == '0' ] || { echo_usage 1>&2; exit; }

# Check number of remaining arguments
[ $# -ge 2 ] || { echo_usage 1>&2; exit; }

# Set directory argument and shift
directory="$1"
shift

# Debug
# echo 'Directory:' "$directory"
# echo 'Make:' "$makedir"
# echo 'Commands:' "$@"
# echo

if [ "$makedir" == '0' ]; then
	cd "$directory" && "$@"
else
	mkdir -p "$directory" && cd "$directory" && "$@"
fi
