#!/usr/bin/env bash
# rchomp - Recursive chomp of all files (recursive) in a directory
[ $# -ge 1 ] || { echo "usage: $0 <path>"; exit; }
find "$1" -type f -print0 | xargs -0 sed -i 's/[ \t]*$//'
