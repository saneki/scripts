#!/usr/bin/env bash

# Script for efficiently hashing some data using hash binaries.
# Uses tee and process substitution for only reading the input data (or file) once.

[ $# -ge 1 ] || { echo "usage: $0 <file>"; exit; }

file="$1"

hashes=( b2sum cksum md5sum sha1sum sha224sum sha256sum sha384sum sha512sum )

subs=""
for name in "${hashes[@]}"; do
	subs="$subs >(_hash ${name})"
done

_hash()
{
	name="$1"
	output="$("$name" 2>/dev/null | cut -d ' ' -f 1)"
	[ -z "$output" ] || printf "%11s %s\n" "$name:" "$output"
}

read -r -d '' cmd <<EOF
cat "$file" | tee $subs 1>/dev/null | sort
EOF

eval "$cmd"
