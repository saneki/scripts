#!/usr/bin/env bash

# example: ./mv_githost gitlab.com some_user git@host.com:my/path some_repo

_REMOTE="mv-githost"
_BRANCH="master"

[ $# -ge 4 ] || { echo "usage: $0 <pull-host> <site-user> <push-path> <repo-name>"; exit; }

# Get a temp directory to clone into
_PATH=$(mktemp -d)

git clone "https://$1/$2/$4.git" "$_PATH" \
&& cd "$_PATH" \
&& git remote add "$_REMOTE" "$3/$4.git" \
&& git push "$_REMOTE" "$_BRANCH" \
&& cd .. \
&& rm -rf "$_PATH"
