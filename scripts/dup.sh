#!/usr/bin/env bash

[[ $# -gt 1 ]] || { echo "usage: $0 <count> [cmd [arg1 [arg2 ...]]]" 1>&2; exit 1; }

count=$1
shift

for ((i=0;i<"${count}";i++)); do ${@}; done
