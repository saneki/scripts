#!/usr/bin/env bash
# Prepends a UTF-8 BOM (byte order mark) to a file

[ $# -ge 2 ] || { echo "usage: $0 <srcfile> <destfile>" 1>&2; exit; }

srcfile="$1"
destfile="$2"

# Expected BOM, maybe add different BOM types as options
bom="$(echo -ne "\xef\xbb\xbf")"

# Read the 3-byte header
header="$(head -c 3 "$srcfile")"
[ $? == 0 ] || { echo "error: unable to read srcfile: $srcfile" 1>&2; exit 1; }

# Check if the header matches the expected BOM
[ "$header" == "$bom" ] && { echo "error: file already has bom: $srcfile" 1>&2; exit 1; }

# Construct dest file
echo -ne "$bom" > "$destfile"
cat "$srcfile" >> "$destfile"
