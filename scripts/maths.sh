#!/usr/bin/env bash

while read -e -p "> " line
do
    [ "$line" == "quit" ] && { exit; }
    RESULT=$(($line))
    printf "0x%x (%d)\n" "$RESULT" "$RESULT"
done < /dev/stdin
