#!/usr/bin/env bash
# Simple script for repeating some output using dd and sed
[ $# -eq 2 ] || { echo "usage: $0 N [text...]"; exit 1; }
dd if=/dev/zero bs="${1}" count=1 2>/dev/null | sed "s/./${2}/g"
