#!/usr/bin/env bash

[ $# -ge 2 ] || { echo "usage: $0 <sharename> <mountpoint>" 1>&2; exit; }

sharename="$1"
mountpoint="$2"

sudo mount -t vboxsf -o uid=$UID,gid=$(id -g) "$sharename" "$mountpoint"
