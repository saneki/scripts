#!/usr/bin/env bash
# See: https://wiki.znc.in/Weechat#Connecting
# Usage: cat cert.pem | weechat-fingerprint.sh

openssl x509 -sha512 -fingerprint -noout | tr -d ':' | tr 'A-Z' 'a-z' | cut -d = -f 2
