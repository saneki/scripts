#!/usr/bin/env bash

# Recursively inflate all gz-compressed files in-place in a directory.

[ $# -ge 1 ] || { echo "usage: $0 <directory>"; exit; }

dir_path="$1"

function inflate_gz_file {
	local file_path="$1"
	local temp_file="$(mktemp)"
	zcat "$file_path" > "$temp_file"
	mv "$temp_file" "$file_path"
}

for file_path in "$dir_path"/**/*; do
	readarray -d ',' -t file_stat < <(stat --printf='%s,%F' "$file_path")
	echo "Path: $file_path, Size: ${file_stat[0]}, Type: \"${file_stat[1]}\""

	if [[ !("${file_stat[1]}" == 'regular file') ]]; then
		echo "File is a directory: $file_path"
	elif [[ "${file_stat[0]}" < 10 ]]; then
		echo "File is too small to be gz compressed: $file_path"
	else
		leading_hex="$(od -A n -N 2 -x "$file_path" | cut -c 2-)"
		if [[ "$leading_hex" == "8b1f" ]]; then
			inflate_gz_file "$file_path"
		fi
	fi
done
